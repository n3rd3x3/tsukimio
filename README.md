# owoify
This is an Xposed module that is supposed to add hiragana to strings of some SystemUI elements. No clue if this is inefficient or not, but it feels like it is. Maybe at some point I'll make it automatically pull the strings, but today is not that day.


## Inspiration
If you can't tell, I forked this from the module owoify, which makes everything owo. I also took inspiration from a user on Telegram who did this by modifying the SystemUI strings in the APK, I'll add a screenshot or link if I ever find the post.

So yeah, glhf.

## Download
Downloads can be found on the releases page, very cool.

## Sources
[KieronQuinn/owoify](https://github.com/KieronQuinn/owoify) for most of the code, the rest is extracted from a Pixel SystemUI APK, this has only been tested on a Pixel 4a 5g running Android 12, no clue if it will be compatible with anything else.

Some of the Xposed implementation code came from the classic [Hodor module](https://github.com/GermainZ/Hodor) by GermainZ, which was used to create owoify, which I used to create this.
