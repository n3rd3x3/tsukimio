package xyz.n3rd3x3.app.tsukimio

import android.text.SpannableStringBuilder
import android.widget.TextView
import android.widget.TextView.BufferType
import de.robv.android.xposed.IXposedHookZygoteInit
import de.robv.android.xposed.IXposedHookZygoteInit.StartupParam
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedHelpers
import kotlin.random.Random

class tsuki : IXposedHookZygoteInit {
    
    /* because i am lazy i am just google translating these strings, once i feel like actually decompiling systemUI i will do so lol
    */

    companion object {

        val suffixes = mapOf(
            Pair("Silent", "しずけさ"),
            Pair("nerd", "オタク")
        )

    }

    /*
        Hook code loosely based off Hodor by GermainZ
        https://github.com/GermainZ/Hodor
     */

    override fun initZygote(hodorHodorHodorHodor: StartupParam) {
        val hook: XC_MethodHook = object : XC_MethodHook() {
            override fun beforeHookedMethod(param: MethodHookParam) {
                var suffix = param.args[0] as CharSequence
                 val output = SpannableStringBuilder().apply {
                        append(" · ")
                        append(suffix)
                    }
                param.args[0] = output as CharSequence
            }
        }
        XposedHelpers.findAndHookMethod(TextView::class.java, "setText", CharSequence::class.java, BufferType::class.java,
                Boolean::class.javaPrimitiveType, Int::class.javaPrimitiveType, hook)
        XposedHelpers.findAndHookMethod(TextView::class.java, "setHint", CharSequence::class.java, hook)
    }
}
